package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

// TODO
//
// ParentEntity 应当具备一个自动生成的 id 以及一个字符串 name。除此之外 ParentEntity 应当包含一个
// List<ChildEntity> 类型的 children 字段以显示 parent 和 child 是一对多的关系。请实现 ParentEntity。
// ParentEntity 的参考数据表定义如下：
//
// parent_entity
// +─────────+──────────────+──────────────────────────────+
// | Column  | Type         | Additional                   |
// +─────────+──────────────+──────────────────────────────+
// | id      | bigint       | primary key, auto_increment  |
// | name    | varchar(20)  | not null                     |
// +─────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class ParentEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name="parent_entity_id")
    private List<ChildEntity> children = new ArrayList<>();


    public ParentEntity(String name) {
        this.name = name;
    }

    public ParentEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
}

    public void setName(String name) {
        this.name = name;
    }

    public List<ChildEntity> getChildren() {
        return children;
    }

    public void setChild(ChildEntity childEntity) {
        children.add(childEntity);
    }

    public List<ChildEntity> getChild() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParentEntity that = (ParentEntity) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

